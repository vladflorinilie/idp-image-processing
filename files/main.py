from flask import Flask, request
import subprocess
import os

app = Flask(__name__)

@app.route('/filter')
def hello():
    location = request.args.get('location')
    flt = request.args.get('filter')

    filename = location.split("-original")
    pnmorig = filename[0] + "-original.pnm"
    pnmmodif = filename[0] + "-modified.pnm"
    jpgmodif = filename[0] + "-modified" + filename[1]

    po = open(pnmorig, "w")
    subprocess.call(["jpegtopnm", location], stdout=po)
    po.close()

    subprocess.call(["./filter", pnmorig, pnmmodif, flt])

    pm = open(jpgmodif, "w")
    subprocess.call(["pnmtojpeg", pnmmodif], stdout=pm)
    pm.close()

    os.remove(pnmorig)
    os.remove(pnmmodif)

    return "Done"

if __name__ == '__main__':
    app.run(host='0.0.0.0')