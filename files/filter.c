#include "filter.h"
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

void allocAndRead(const char *fileName, image *img, int justAlloc) {
	int i, j;
	char aux;
	FILE *in;
	
	if (!justAlloc) {
		in = fopen(fileName, "r");
		if (in == NULL) {
			exit(-1);
		}
	
		fscanf(in, "%c%c\n", &aux, &(img->type));
		fscanf(in, "%d %d\n", &(img->width), &(img->height));
		fscanf(in, "%d\n", &(img->maxval));
	}
	
	if (img->type == '5') { // check image type
		// allocate the matrix
		img->image = (void**) malloc(sizeof(imageGreyScale*) * img->height);
		if (img->image == NULL) {
			exit(-1);
		}

		for (i = 0; i < img->height; i++) {
			img->image[i] = (void*) malloc(sizeof(imageGreyScale) * img->width);
			if (img->image[i] == NULL) {
				for (j = 0; j < i; j++) {
					free(img->image[j]);
				}
				free(img->image);
				exit(-1);
			}
			
			if (!justAlloc) {
				fread(img->image[i], img->width, sizeof(imageGreyScale), in);
			}
		}
	} else if (img->type == '6') {
		img->image = (void**) malloc(sizeof(imageRGB*) * img->height);
		if (img->image == NULL) {
			exit(-1);
		}

		for (i = 0; i < img->height; i++) {
			img->image[i] = (void*) malloc(sizeof(imageRGB) * img->width);
			if (img->image[i] == NULL) {
				for (j = 0; j < i; j++) {
					free(img->image[j]);
				}
				free(img->image);
				exit(-1);
			}
			
			if (!justAlloc) {
				fread(img->image[i], img->width, sizeof(imageRGB), in);
			}
		}
	}
	
	if (!justAlloc) {
		fclose(in);
	}
}

void getFilter(char *filterName, int *div, float (*flt)[3]) { // copies the filter in the flt var
	if (strcmp(filterName, "smooth") == 0) {
		*div = 9;
		memcpy(flt, smooth, 9 * sizeof(float));
	} else if (strcmp(filterName, "blur") == 0) {
		*div = 16;
		memcpy(flt, gaussian, 9 * sizeof(float));
	} else if (strcmp(filterName, "sharpen") == 0) {
		*div = 3;
		memcpy(flt, sharp, 9 * sizeof(float));
	} else if (strcmp(filterName, "mean") == 0) {
		*div = 1;
		memcpy(flt, meanrem, 9 * sizeof(float));
	} else if (strcmp(filterName, "emboss") == 0) {
		*div = 1;
		memcpy(flt, emboss, 9 * sizeof(float));
	}
}

void compute(image *in, image *out, char *filterName) { // apply the filter on the image
	int divisor;
	float filter[3][3];
	getFilter(filterName, &divisor, filter);
	
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			filter[i][j] /= divisor; // firstly, divide the elements of the filter by the divisor
		}
	}
	
	int elemPerPixel = (in->type == '5' ? 1 : 3); // number of matrix elements that form a pixel
		
	for (int i = 1; i < in->height - 1; i++) {
		for (int j = 1; j < in->width - 1; j++) {
			if (in->type == '5') { // apply the filter on the pixels of a greyscale image
				imageGreyScale *lineS = (imageGreyScale*) in->image[i - 1];
				imageGreyScale *lineM = (imageGreyScale*) in->image[i];
				imageGreyScale *lineJ = (imageGreyScale*) in->image[i + 1];
				imageGreyScale *outLine = (imageGreyScale*) out->image[i];
					
				float g = 0;
				g += lineS[j - 1].g * filter[0][0];
				g += lineS[j].g * filter[0][1];
				g += lineS[j + 1].g * filter[0][2];
					
				g += lineM[j - 1].g * filter[1][0];
				g += lineM[j].g * filter[1][1];
				g += lineM[j + 1].g * filter[1][2];
					
				g += lineJ[j - 1].g * filter[2][0];
				g += lineJ[j].g * filter[2][1];
				g += lineJ[j + 1].g * filter[2][2];
					
				outLine[j].g = g;
			} else { // apply the filter on a color image
				imageRGB *lineS = (imageRGB*) in->image[i - 1];
				imageRGB *lineM = (imageRGB*) in->image[i];
				imageRGB *lineJ = (imageRGB*) in->image[i + 1];
				imageRGB *outLine = (imageRGB*) out->image[i];
					
				float r = 0, g = 0, b = 0;
				g += lineS[j - 1].g * filter[0][0];
				g += lineS[j].g * filter[0][1];
				g += lineS[j + 1].g * filter[0][2];
					
				g += lineM[j - 1].g * filter[1][0];
				g += lineM[j].g * filter[1][1];
				g += lineM[j + 1].g * filter[1][2];
					
				g += lineJ[j - 1].g * filter[2][0];
				g += lineJ[j].g * filter[2][1];
				g += lineJ[j + 1].g * filter[2][2];
					
				outLine[j].g = g;
					
				r += lineS[j - 1].r * filter[0][0];
				r += lineS[j].r * filter[0][1];
				r += lineS[j + 1].r * filter[0][2];
				
				r += lineM[j - 1].r * filter[1][0];
				r += lineM[j].r * filter[1][1];
				r += lineM[j + 1].r * filter[1][2];
					
				r += lineJ[j - 1].r * filter[2][0];
				r += lineJ[j].r * filter[2][1];
				r += lineJ[j + 1].r * filter[2][2];
					
				outLine[j].r = r;
					
				b += lineS[j - 1].b * filter[0][0];
				b += lineS[j].b * filter[0][1];
				b += lineS[j + 1].b * filter[0][2];
					
				b += lineM[j - 1].b * filter[1][0];
				b += lineM[j].b * filter[1][1];
				b += lineM[j + 1].b * filter[1][2];
					
				b += lineJ[j - 1].b * filter[2][0];
				b += lineJ[j].b * filter[2][1];
				b += lineJ[j + 1].b * filter[2][2];
					
				outLine[j].b = b;
			}
		}
	}
}

void writeAndFree(const char *fileName, image *img, int justFree) {
	int i;
	FILE *out;
	
	if (!justFree) {
		out = fopen(fileName, "w");
		
		fprintf(out, "P%c\n", img->type); // write the header in the file
		fprintf(out, "%d %d\n", img->width, img->height);
		fprintf(out, "%d\n", img->maxval);
	}
	
	for (i = 0; i < img->height; i++) {
		if (img->type == '5' && !justFree) {
			fwrite(img->image[i], img->width, sizeof(imageGreyScale), out);
		} else if (img->type == '6' && !justFree) {
			fwrite(img->image[i], img->width, sizeof(imageRGB), out);
		}
		free(img->image[i]);
	}
	free(img->image);

	if (!justFree) {
		fclose(out);
	}
}

int main(int argc, char * argv[]) {
	image *in, *out, *final, *other;
	
	in = (image*) malloc(sizeof(image));
	out = (image*) malloc(sizeof(image));
	
   allocAndRead(argv[1], in, 0); // read entire image from file
   int elemPerPixel = (in->type == '5' ? 1 : 3);
   
   out->type = in->type;
   out->width = in->width;
   out->height = in->height;
   out->maxval = in->maxval;
   
   allocAndRead(NULL, out, 1); // images used in the process
	
	for (int k = 3; k < argc; k++) { // for each filter
		compute(in, out, argv[k]);
		
		image *aux;
		aux = in; // switch the images
		in = out;
		out = aux;
      final = in;
      other = out;
	}

   writeAndFree(argv[2], final, 0); // write the final image to the file
   writeAndFree(NULL, other, 1);
	free(in);
	free(out);
	
	return 0;
}