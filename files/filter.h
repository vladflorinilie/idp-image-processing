#ifndef FILTER_H
#define FILTER_H

#pragma pack(1)
typedef struct {
	unsigned char r;
	unsigned char g;
	unsigned char b;
} imageRGB;

typedef struct {
	unsigned char g;
} imageGreyScale;
#pragma pack()

typedef struct {
	// header imagine
	char type;
	int width;
	int height;
	int maxval;
	// imaginea (imageRGB sau imageGreyScale)
	void **image;
} image;

void allocAndRead(const char *fileName, image *img, int justAlloc);

void writeAndFree(const char *fileName, image *img, int justFree);

void getFilter(char *filterName, int *div, float (*flt)[3]);

void compute(image *in, image *out, char *filterName);

float smooth[3][3] = 
{
    {1, 1, 1},
    {1, 1, 1},
    {1, 1, 1}
};

float gaussian[3][3] = 
{
    {1, 2, 1},
    {2, 4, 2},
    {1, 2, 1}
};

float sharp[3][3] = 
{
    {0, -2, 0},
    {-2, 11, -2},
    {0, -2, 0}
};

float meanrem[3][3] = 
{
    {-1, -1, -1},
    {-1, 9, -1},
    {-1, -1, -1}
};

float emboss[3][3] = 
{
    {0, 1, 0},
    {0, 0, 0},
    {0, -1, 0}
};

#endif /* FILTER_H */