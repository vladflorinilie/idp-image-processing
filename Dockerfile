FROM python:3.7-stretch

ADD files /opt/imageprocessing

WORKDIR /opt/imageprocessing

RUN pip install -r requirements.txt

RUN apt-get update

RUN apt-get install -y netpbm

RUN gcc filter.c -o filter -lm

ENTRYPOINT ["python", "main.py"]